from rest_framework.views import APIView
from django.http import JsonResponse


class SpeechRecognizer(APIView):

    def post(self, request):
        input_data = request.data['data']
        return JsonResponse({'result': 'Слава украини! Входящие данные: {}'.format(input_data)})

