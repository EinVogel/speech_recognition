from django.urls import path
from .views import SpeechRecognizer

urlpatterns = [
    path('', SpeechRecognizer.as_view()),
    ]
