FROM python:3.6

# задаёт рабочую директорию для следующих инструкции
WORKDIR /speech_api

# копирует в контейнер папку spech_api в контейнер
COPY ./speech_api /speech_api
# Установка зависимостей
RUN pip install -r requirements.txt
# Применение миграций
# RUN python manage.py makemigrations
# RUN python manage.py migrate

# CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]
